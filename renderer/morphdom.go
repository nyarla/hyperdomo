// +build js

package renderer

import (
	"golang.org/x/net/html"
	"strings"

	"github.com/gopherjs/gopherjs/js"
	"github.com/nyarla/hypercode/describers"
)

type Options struct {
	GetNodeKey                     func(node *js.Object) string
	OnBeforeNodeAdded              func(node *js.Object) bool
	OnNodeAdded                    func(node *js.Object)
	OnBeforeElementUpdated         func(fromElement, toElement *js.Object) bool
	OnElemenetUpdated              func(node *js.Object)
	OnBeforeNodeDiscarded          func(node *js.Object)
	OnBeforeElementChildrenUpdated func(fromElement, toElement *js.Object) bool
	ChildrenOnly                   bool
}

func (o Options) ToJSObject() *js.Object {
	return js.MakeWrapper(js.M{
		`getNodeKey`:                o.GetNodeKey,
		`onBeforeNodeAdded`:         o.OnBeforeNodeAdded,
		`onNodeAdded`:               o.OnNodeAdded,
		`onBeforeElUpdated`:         o.OnBeforeElementUpdated,
		`onElUpdated`:               o.OnElemenetUpdated,
		`onBeforeNodeDiscarded`:     o.OnBeforeNodeDiscarded,
		`onBeforeElChildrenUpdated`: o.OnBeforeElementChildrenUpdated,
		`childrenOnly`:              o.ChildrenOnly,
	})
}

type Renderer struct {
	container *js.Object
	view      describers.Describer
}

func New(container *js.Object, view describers.Describer) Renderer {
	return Renderer{
		container: container,
		view:      view,
	}
}

func (r Renderer) Render() {
	node := new(html.Node)
	r.view.Describe(node)

	var patch strings.Builder
	html.Render(&patch, node)

	js.Global.Get(`window`).Call(`morphdom`, r.container, patch.String())
}

func (r Renderer) RenderWithOptions(opts Options) {
	node := new(html.Node)
	r.view.Describe(node)

	var patch strings.Builder
	html.Render(&patch, node)

	js.Global.Get(`window`).Call(`morphdom`, r.container, patch.String(), opts.ToJSObject())
}
