package event

import (
	"testing"
)

func TestDefaultName(t *testing.T) {
	if n := Name(`click`); n != `$hyperdomo$click$` {
		t.Fatalf(`'%s' != '$hyperdomo$click$'`, n)
	}
}

func TestDefaultScript(t *testing.T) {
	ret := `;(function () { window.dispatchEvent( new Event('$hyperdomo$click$') ) })();`
	if s := Script(Name(`click`)); s != ret {
		t.Fatal(s, ret)
	}
}
