package event

import (
	"fmt"
	"html"
)

var (
	NameTpl   = `$hyperdomo$%s$`
	ScriptTpl = `;(function () { window.dispatchEvent( new Event('%s') ) })();`
)

func Name(name string) string {
	return fmt.Sprintf(NameTpl, name)
}

func Script(name string) string {
	return fmt.Sprintf(ScriptTpl, html.EscapeString(name))
}
