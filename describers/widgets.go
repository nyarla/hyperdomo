package describers

import (
	"github.com/nyarla/hypercode/describers"
	"golang.org/x/net/html"
)

type WidgetsDescriber func() describers.Describer

func (w WidgetsDescriber) Describe(target *html.Node) error {
	return w().Describe(target)
}
