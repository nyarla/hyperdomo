package describers

import (
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"strings"
	"testing"
)

func TestEventsDescriber(t *testing.T) {
	desc := EventsDescriber{`onclick`: `clickEvent`}
	node := &html.Node{
		Type:     html.ElementNode,
		DataAtom: atom.Div,
		Data:     `div`,
	}

	if err := desc.Describe(node); err != nil {
		t.Fatal(`failed to describe to html.Node tree by event handlers describer.`)
	}

	b := new(strings.Builder)
	html.Render(b, node)

	if b.String() != `<div onclick=";(function () { document.body.dispatchEvent( new Event(&#39;$hyperdomo$clickEvent$&#39;) ) })();"></div>` {
		t.Fatal(b.String())
	}
}
