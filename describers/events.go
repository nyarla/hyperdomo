package describers

import (
	"golang.org/x/net/html"

	"github.com/nyarla/hyperdomo/event"
)

type EventsDescriber map[string]string

func (e EventsDescriber) Describe(target *html.Node) error {
events:
	for ev, fn := range e {
		code := event.Script(event.Name(fn))

		for idx, attr := range target.Attr {
			if attr.Key == ev {
				target.Attr[idx].Val = code
				continue events
			}
		}

		target.Attr = append(target.Attr, html.Attribute{Key: ev, Val: code})
	}

	return nil
}
