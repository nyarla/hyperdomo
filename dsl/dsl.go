package dsl

import (
	"github.com/nyarla/hypercode/describers"
	"github.com/nyarla/hypercode/dsl"
	custom "github.com/nyarla/hyperdomo/describers"
	"golang.org/x/net/html"
)

type A map[string]string

func (a A) Describe(target *html.Node) error {
	return dsl.A(a).Describe(target)
}

type T string

func (t T) Describe(target *html.Node) error {
	return dsl.T(t).Describe(target)
}

func H(tag string, contents ...describers.Describer) describers.Describer {
	return dsl.H(tag, contents...)
}

type E map[string]string

func (e E) Describe(target *html.Node) error {
	return custom.EventsDescriber(e).Describe(target)
}

type W func() describers.Describer

func (w W) Describe(target *html.Node) error {
	return w().Describe(target)
}
