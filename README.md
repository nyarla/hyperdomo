hyperdomo
=========

  * A micro web application framework for frontend with GopherJS

SYNOPSIS
--------


### `_examples/index.html`

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>hyperdomo example</title>
  </head>
  <body>
    <main></main>
    <script src="./_examples.js"></script>
  </body>
</html>
```

### `_examples/main.go`

```go
package main

import (
	"errors"
	"fmt"

	"github.com/nyarla/hypercode/describers"
	"github.com/nyarla/hyperdomo/app"
	. "github.com/nyarla/hyperdomo/dsl"
)

type State struct {
	value int64
}

func (s *State) Update(name string) error {
	switch name {
	case `up`:
		s.value = s.value + 1
	case `down`:
		s.value = s.value - 1
	default:
		return errors.New(`invalid action name.`)
	}

	return nil
}

func (s *State) Widget() describers.Describer {
	return W(func() describers.Describer {
		text := fmt.Sprintf(`count: %d`, s.value)
		return T(text)
	})
}

func main() {
	state := new(State)
	view := H(`main`,
		H(`p`, state.Widget()),
		H(`button`, T(`UP`), E{`onclick`: `up`}),
		H(`button`, T(`DOWN`), E{`onclick`: `down`}),
	)

	mainApp := new(app.Application)
	mainApp.EntryPoint = `main`
	mainApp.View = view
	mainApp.Actions = app.Actions{
		`up`:   func() { state.Update(`up`) },
		`down`: func() { state.Update(`down`) },
	}

	mainApp.Mount()
}
```

LICENSE
-------

```
The MIT License (MIT)

Copyright © 2018 Naoki OKAMURA <nyarla@thotep.net>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```

