package main

import (
	"errors"
	"fmt"

	"github.com/nyarla/hypercode/describers"
	"github.com/nyarla/hyperdomo/app"
	. "github.com/nyarla/hyperdomo/dsl"
)

type State struct {
	value int64
}

func (s *State) Update(name string) error {
	switch name {
	case `up`:
		s.value = s.value + 1
	case `down`:
		s.value = s.value - 1
	default:
		return errors.New(`invalid action name.`)
	}

	return nil
}

func (s *State) Widget() describers.Describer {
	return W(func() describers.Describer {
		text := fmt.Sprintf(`count: %d`, s.value)
		return T(text)
	})
}

func main() {
	state := new(State)
	view := H(`main`,
		H(`p`, state.Widget()),
		H(`button`, T(`UP`), E{`onclick`: `up`}),
		H(`button`, T(`DOWN`), E{`onclick`: `down`}),
	)

	mainApp := new(app.Application)
	mainApp.EntryPoint = `main`
	mainApp.View = view
	mainApp.Actions = app.Actions{
		`up`:   func() { state.Update(`up`) },
		`down`: func() { state.Update(`down`) },
	}

	mainApp.Mount()
}
