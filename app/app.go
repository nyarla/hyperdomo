// +build js

package app

import (
	"github.com/gopherjs/gopherjs/js"
	"github.com/nyarla/hypercode/describers"

	"github.com/nyarla/hyperdomo/event"
	"github.com/nyarla/hyperdomo/renderer"
)

type Actions map[string]func()

type Options renderer.Options

type Application struct {
	EntryPoint string
	View       describers.Describer
	Actions    Actions
	Options    Options
}

func (app *Application) mkEventHandler(action func(), dom renderer.Renderer) *js.Object {
	code := (func(action func(), dom renderer.Renderer) func(*js.Object, []*js.Object) interface{} {
		return func(_ *js.Object, _ []*js.Object) interface{} {
			go func(fn func(), r renderer.Renderer) {
				fn()
				r.Render()
			}(action, dom)
			return nil
		}
	})(action, dom)

	return js.MakeFunc(code)
}

func (app *Application) Mount() {
	window := js.Global.Get(`window`)
	target := js.Global.Get(`document`).Call(`querySelector`, app.EntryPoint)

	dom := renderer.New(target, app.View)

	for name, action := range app.Actions {
		window.Call(`addEventListener`, event.Name(name), app.mkEventHandler(action, dom))
	}

	dom.Render()
}
